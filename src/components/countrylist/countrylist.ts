import { Component, Input } from '@angular/core';

/**
 * Generated class for the CountrylistComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'countrylist',
  templateUrl: 'countrylist.html'
})
export class CountrylistComponent {

  @Input() list: any;

  constructor() {
    console.log('Hello CountrylistComponent Component');
  }
}
