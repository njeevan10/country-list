import { NgModule } from '@angular/core';
import { CountrylistComponent } from './countrylist/countrylist';
import { IonicModule } from 'ionic-angular';
@NgModule({
	declarations: [CountrylistComponent],
	imports: [IonicModule],
	exports: [CountrylistComponent]
})
export class ComponentsModule {}
