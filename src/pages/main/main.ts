import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
import { RemoteServiceProvider, Country } from '../../providers/remote-service/remote-service';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';


/**
 * Generated class for the MainPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
})



export class MainPage {

  countryLst: Country[] = [];
  hasValue: boolean = false;
  loader: any;
  isDataCopied: boolean = false;

  constructor(public navCtrl: NavController,
    private remoteService: RemoteServiceProvider,
    private sqlite: SQLite,
    public loadingCtrl: LoadingController) {

  }

  ionViewWillEnter() {
    this.isDataCopied = false;
    this.checkDataExistance();
  }

  /**
   * Check whether table has any data
   * If Yes, fetch data from table and send counry data to child component
   * Else, Create table and load data from server through API and save it into SQLlite
   */
  checkDataExistance() {
    this.sqlite.create({
      name: 'country.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        db.executeSql('SELECT * FROM country_list', [])
          .then((res) => {

            let count: number = res.rows.length;
            this.hasValue = count > 0 ? true : false;

            if (this.hasValue) {
              this.loader = this.loadingCtrl.create({
                content: "Please wait Loading country data from local..."
              })
              this.loader.present();
            } else {
              this.cretaeTable();
            }


            this.countryLst = [];
            for (var i = 0; i < count; i++) {

              let obj: Country = new Country();
              obj.name = res.rows.item(i).name;
              obj.region = res.rows.item(i).region;
              obj.capital = res.rows.item(i).capital;
              obj.flag = res.rows.item(i).flag;
              obj.population = res.rows.item(i).population;

              this.countryLst.push(obj);

              if (i == (count - 1)) {
                this.loader.dismiss();
                this.isDataCopied = true;
              }
            }
          })
          .catch(e => {
            console.log(e);
            this.hasValue = false;
            this.cretaeTable();

          });
      })
      .catch(e => console.log(e));
  }

  /**
   *  Create table in country db only if table is not exist
   */
  cretaeTable() {
    this.sqlite.create({
      name: 'country.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        db.executeSql('CREATE TABLE IF NOT EXISTS country_list(id INTEGER PRIMARY KEY, name TEXT, region TEXT, capital TEXT, flag TEXT,population INT)', [])
          .then(() => {
            this.loadCountryData();
          })
          .catch(e => console.log(e));
      })
      .catch(e => console.log(e));
  }

  /**
   *  Save data from API to SQLlite
   */
  saveCountryData(data: Country[]) {
    this.sqlite.create({
      name: 'country.db',
      location: 'default'
    })
      .then((db: SQLiteObject) => {

        for (var i = 0; i < data.length; i++) {
          let obj: Country = data[i];
          db.executeSql('INSERT INTO country_list VALUES(?,?,?,?,?,?)', [i, obj.name, obj.region, obj.capital, obj.flag, obj.population])
            .then(res => {
              //console.log(res);
            })
            .catch(e => {
              console.log(e);
            });
        }

      })
      .catch(e => console.log(e));
  }

  /**
   *  Call API to load country details
   */
  loadCountryData() {
    this.loader = this.loadingCtrl.create({
      content: "Please wait.. loading country data from server"
    })
    this.loader.present();

    this.remoteService.getCountryData().subscribe((data: any) => {
      this.countryLst = data;
      this.loader.dismiss();
      this.saveCountryData(data)
    },
      (error: any) => {
        console.log(error);
        this.loader.dismiss();
      });
  }




}