import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { RemoteServiceProvider } from '../providers/remote-service/remote-service';
import { HttpClientModule } from '@angular/common/http';

import { SQLite } from '@ionic-native/sqlite';
import { ComponentsModule } from '../components/components.module';
import { MainPage } from '../pages/main/main';

@NgModule({
  declarations: [
    MyApp, MainPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ComponentsModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MainPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    RemoteServiceProvider,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    SQLite,
  ]
})
export class AppModule { }
