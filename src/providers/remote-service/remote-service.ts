import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
/* import 'rxjs/add/operator/do';


import 'rxjs/add/observable/throw';
 */
// import 'rxjs/add/operator/catch';
// import 'rxjs/add/operator/map';
// import { Observable } from 'rxjs/Observable';

import {Observable} from 'rxjs';

/*
  Generated class for the RemoteServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/

export class Country {
  name: string;
  region: string;
  capital: string;
  population: number;
  flag: string;
  constructor(values: Object = {}) {
    Object.assign(this, values);
  }
}

@Injectable()
export class RemoteServiceProvider {

  API_COUNTRY: string = "https://restcountries.eu/rest/v2/all";

  constructor(public http: HttpClient) {
    console.log('Hello RemoteServiceProvider Provider');
  }

  public getCountryData(): Observable<Country[]> {
    return this.http
      .get(this.API_COUNTRY)
      .map(products => {
        return products
      })
      .catch((err:any)=>{
        return Observable.throw(err);
      });
  }
}
